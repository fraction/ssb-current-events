const schedule = require('./lib/schedule.js')
const { timeLeft, getDateData } = require('./lib/date.js')
const convert = require('./lib/convert.js')
const publish = require('./lib/publish.js')
const download = require('./lib/download.js')

const PRODUCTION = process.env.PRODUCTION || false
const MANUAL = process.env.MANUAL || false

const left = timeLeft(PRODUCTION)
console.log(`${left.hours}:${left.minutes}:${left.seconds} remaining`)

schedule(PRODUCTION, () => {
  const { dateString, isoDateString } = getDateData()
  const query = `Portal:Current events/${dateString}`
  const queryUnderscored = query.split(' ').join('_')
  const header = `# ${isoDateString}\n`
  const footer = `\n---\n\nhttps://en.wikipedia.org/w/index.php?title=${queryUnderscored}`
  download(query).then((html) => {
    const text = [header, convert(html), footer].join('\n')

    if (MANUAL || PRODUCTION) {
      let cb = null
      if (MANUAL) {
        cb = process.exit
      }
      publish(text, cb)
    } else {
      console.log(text)
      console.log('Note: you are in test mode. Use `npm start` to run in production')
    }
  })
})
