# ssb-current-events

*Current Events* from Wikipedia, every day at 00:07.

## Installation

Clone and satisfy NPM dependencies.

```sh
git clone https://gitlab.com/christianbundy/ssb-current-events.git
cd ssb-current-events
npm ci
```

## Usage

You'll need an ssb-server (or similar) running in the background **with the
ssb-identities plugin installed**. You can install with:

```shell
ssb-server plugins.install --name ssb-identities
```

In a perfect world you'd only need to restart your ssb-server, but you may need
to [manually edit your config](https://github.com/ssbc/ssb-server/issues/675#issuecomment-514343535).

You probably want some Markdown output every minute or so for testing.

```sh
npm test
```

Ready for production? If you have a key it will be used. Otherwise it will be
created for you and stored in `~/.ssb/current-events.txt`. Either way, you
shouldn't have to worry about it.

```sh
npm start
```

Bingo bango bongo, you're doing the thing.

## Support

Please [open an issue][0] for support.

## Contributing

Please contribute using Git. Add commit to a branch, and [open a merge request][1].

[0]: https://gitlab.com/christianbundy/ssb-current-events/issues/new
[1]: https://gitlab.com/christianbundy/ssb-current-events/merge_requests/new
