const connect = require('./lib/connect')

connect().then(({ publish, id }) => {
  publish({
    type: 'about',
    about: id,
    name: 'Current Events'
  }, (err, msg) => {
    if (err) throw err
    console.log(msg)
  })
}).catch(console.error)
