const { job } = require('cron')

const schedule = (PRODUCTION, cb) => {
  let cronString
  if (PRODUCTION) {
    cronString = '7 0 * * *'
  } else {
    cronString = '* * * * *'
  }

  job(cronString, cb, null, true, 'UTC')
}

module.exports = schedule
