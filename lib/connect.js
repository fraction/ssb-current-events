const path = require('path')
const ssbClient = require('ssb-client')
const { homedir } = require('os')
const { promisify } = require('util')
const { readFile, writeFileSync } = require('fs')

const createPublish = (publishAs, id) => (content, cb) => {
  publishAs({ id, content }, cb)
}

const readFilePromise = promisify(readFile)

module.exports = () => new Promise((resolve, reject) => {
  ssbClient((err, ssb) => {
    if (err) {
      return reject(err)
    }
    if (ssb.identities == null) {
      return reject(new Error('must install ssb-identities'))
    }

    const filePath = path.join(homedir(), '.ssb', 'current-events.txt')

    readFilePromise(filePath, { encoding: 'utf8' }).then((fileText) => {
      const id = fileText.replace('\n', '')
      const publish = createPublish(ssb.identities.publishAs, id)
      resolve({ publish, id })
    }).catch((err) => {
      if (err) console.error('Probably harmless:', err)

      ssb.identities.create((err, id) => {
        if (err) return reject(err)
        writeFileSync(filePath, id)
        const publish = createPublish(ssb.identities.publishAs, id)
        resolve({ publish, id })
      })
    })
  })
})
