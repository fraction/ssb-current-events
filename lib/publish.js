const connect = require('./connect')

module.exports = (text, cb) => {
  connect().then(({ publish, id }) => {
    publish({
      type: 'post',
      channel: 'current-events',
      text
    }, (err, msg) => {
      if (err) console.error(err)
      console.log(msg)
      if (cb) cb()
    })
  })
}
