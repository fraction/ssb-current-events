# Changelog

## v2.0.0

- Switch from deprecated ssb-feed to ssb-identities
- Upgrade all npm modules
- Switch to StandardJS linter
